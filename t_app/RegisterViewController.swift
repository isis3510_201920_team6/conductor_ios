
//
//  AuthViewController.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO and JAVIER SANTIAGO BARBOSA CASTILLO on 10/30/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    var name: String?
    var phone: Int?
    var id: Int?
    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var txtMail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addConstraints([
        NSLayoutConstraint(item: txtMail as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: txtMail as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.5, constant: 0),
        NSLayoutConstraint(item: txtPassword as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: txtPassword as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.75, constant: 0),
        NSLayoutConstraint(item: txtRPassword as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: txtRPassword as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: btnAccept as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: btnAccept as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.75, constant: 0)
        ])
        
        btnAccept.addTarget(self, action: #selector(signUpAction), for: .touchUpInside)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        self.txtMail.delegate = self
        self.txtPassword.delegate = self
        self.txtRPassword.delegate = self
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func signUpAction(sender: UIButton!) {
        if(CheckInternet.Connection()) {
            if txtPassword.text != txtRPassword.text {
                let alertController = UIAlertController(title: "Las contraseñas no coinciden", message: "Por favor vuelva a ingresarlas", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
            else if !txtMail.text!.contains("@") {
                Auth.auth().createUser(withEmail: txtMail.text!, password: txtPassword.text!){ (user, error) in
                    if error == nil {
                        Firestore.firestore().collection("drivers").addDocument(data: [
                            "userID": Auth.auth().currentUser?.uid as Any,
                            "name": self.name!,
                            "id": self.id!,
                            "future_trips": Array<Trip>(),
                            "past_trips": Array<Trip>(),
                            "phone_number": self.phone!,
                            "profile_pic": "gs://t-app-31605.appspot.com/profile_picture.png",
                            "score": 5.0,
                        ])
                        globalDriver.updateDriver()
                        let alertController = UIAlertController(title: "Registro exitoso", message: "Su cuenta ha sido creada.", preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                            self.performSegue(withIdentifier: "register_to_tabs", sender: UITabBarController.self)
                        }))
                        self.present(alertController, animated: true, completion: nil)
                    }
                    else {
                        let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                        let defaultAction = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
            else {
                let alertController = UIAlertController(title: "El correo electrónico no es válido", message: "Por favor vuelva a ingresarlo", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
                
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        else {
            let alertController = UIAlertController(title: "Error en la red", message: "Por favor revise su conexión a internet e intente nuevamente.", preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func setUserData(_ name: String, _ id: String, _ phone: String) {
        self.name = name
        self.id = Int(id) ?? 0
        self.phone = Int(phone) ?? 0
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
