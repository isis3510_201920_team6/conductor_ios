//
//  ViewController.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 10/4/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class WelcomeViewController: UIViewController {

    @IBOutlet weak var imageWelcome: UIImageView!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    var handle: NSObjectProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnLogin.layer.cornerRadius = 20
        btnLogin.layer.borderWidth = 2
        btnLogin.layer.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        btnLogin.layer.borderColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        btnLogin.setTitleColor(UIColor.white, for: .normal)
        btnRegister.layer.cornerRadius = 20
        btnRegister.layer.borderWidth = 2
        btnRegister.layer.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        btnRegister.layer.borderColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
        btnRegister.setTitleColor(UIColor.white, for: .normal)
        
        view.addConstraints([
            NSLayoutConstraint(item: btnLogin as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: btnLogin as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.35, constant: 0),
            NSLayoutConstraint(item: btnRegister as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: btnRegister as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.65, constant: 0),
            NSLayoutConstraint(item: imageWelcome as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: imageWelcome as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.5, constant: 0)])
        let welcomeImg = UIImage(named: "logo_bus.jpg")
        imageWelcome.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: imageWelcome.frame.width, height: view.frame.height ))
        imageWelcome.layer.cornerRadius = 15
        UIGraphicsBeginImageContext(CGSize(width: imageWelcome.frame.width, height: imageWelcome.frame.height*0.8))
        welcomeImg!.draw(in: CGRect(x: 0, y: 0, width: imageWelcome.frame.width, height: imageWelcome.frame.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        imageWelcome.image = newImage
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
          // ...
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(handle!)
    }

}

