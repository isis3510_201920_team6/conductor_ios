//
//  Driver.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 25/11/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore
import FirebaseAuth

class Driver {
    
    private(set) var name: String?
    private(set) var id: NSNumber?
    private(set) var pastTrips: Array<Trip>?
    private(set) var pastReferences: Array<DocumentReference>?
    private(set) var futureTrips: Array<Trip>?
    private(set) var futureReferences: Array<DocumentReference>?
    private(set) var phoneNumber: NSNumber?
    private(set) var profilePic: String?
    private(set) var score: Double?
    private var db: Firestore
    private let notificationCenter: NotificationCenter
    private let queue = DispatchQueue(label: "driverUpdater", qos: .userInitiated)
    
    
    init(notificationCenter: NotificationCenter = .default) {
        db = Firestore.firestore()
        self.notificationCenter = notificationCenter
    }
    
    func toString() -> String? {
        
        var string = ""
        string = (name ?? "") + " "
        string += (id?.stringValue ?? "") + " "
        string += (pastReferences?.description ?? "") + " "
        string += (futureReferences?.description ?? "") + " "
        string += (phoneNumber?.stringValue ?? "") + " "
        string += (profilePic ?? "") + " "
        string += (score?.description ?? "")
        return string
        
    }
    
    private func setDriver(_ driver: [String : Any?]) {
        for attr in driver {
            switch attr.key {
            case "name":
                self.name = attr.value as? String
            case "id":
                self.id = attr.value as? NSNumber
            case "future_trips":
                self.futureReferences = attr.value as? [DocumentReference]
            case "past_trips":
                self.pastReferences = attr.value as? [DocumentReference]
            case "phone_number":
                self.phoneNumber = attr.value as? NSNumber
            case "profile_pic":
                self.profilePic = attr.value as? String
            case "score":
                self.score = attr.value as? Double
            default:
                break
            }
        }
        
    }
    
    func updateDriver() {
        self.notificationCenter.post(name: Notification.Name(rawValue: driverUpdating), object: nil)
        queue.sync {
            self.db.collection("drivers").whereField("userID", isEqualTo: Auth.auth().currentUser!.uid).getDocuments(completion: { (querySnapshot, error) in
                self.setDriver( querySnapshot?.documents.first?.data() ?? ["":nil] )
                self.pastTrips = []
                var i = 0; while(i<self.pastReferences!.count) {
                    self.addPastTrip(self.pastReferences![i])
                    i+=1
                }
                self.futureTrips = []
                i = 0; while(i<self.futureReferences!.count) {
                    self.addFutureTrip(self.futureReferences![i])
                    i+=1
                }
                self.notificationCenter.post(name: Notification.Name(rawValue: driverUpdated), object: nil)
            })
        }
    }
    
    private func addPastTrip(_ reference: DocumentReference) {
        queue.async {
            reference.getDocument(completion: { (querySnapshot, error ) in
                let tempTrip = Trip()
                tempTrip.setTrip(querySnapshot?.data() ?? ["":nil] )
                self.pastTrips!.append(tempTrip)
                if(self.pastTrips!.count == self.pastReferences!.count) {
                    self.notificationCenter.post(name: Notification.Name(rawValue: pastTripsUpdated), object: nil)
                }
            })
        }
    }
    
    private func addFutureTrip(_ reference: DocumentReference) {
        queue.async {
            reference.getDocument(completion: { (querySnapshot, error ) in
                let tempTrip = Trip()
                tempTrip.setTrip(querySnapshot?.data() ?? ["":nil] )
                self.futureTrips!.append(tempTrip)
                if(self.futureTrips!.count == self.futureReferences!.count) {
                    self.notificationCenter.post(name: Notification.Name(rawValue: futureTripsUpdated), object: nil)
                }
            })
        }
    }
}
