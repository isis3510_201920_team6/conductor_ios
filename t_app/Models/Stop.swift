//
//  Stop.swift
//  t_app
//
//  Created by Mechas Zambrano on 7/12/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import Mapbox

class Stop {
    var location: CLLocationCoordinate2D
    var position: Int?
    
    init() {
        location = CLLocationCoordinate2D()
    }
    
    init(_ coordinates: CLLocationCoordinate2D, _ order: Int) {
        location = coordinates
        position = order
    }
    
    func setStop(_ stop: [String:Any?]) {
        for attr in stop {
            switch attr.key {
            case "lat":
                location.latitude = attr.value as! Double
            case "lon":
                location.longitude = attr.value as! Double
            case "stop_seq_number":
                position = attr.value as? Int
            default:
                break
            }
        }
    }
}
