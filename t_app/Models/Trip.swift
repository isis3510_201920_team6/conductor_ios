//
//  Trip.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 10/22/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import Mapbox
import MapboxCoreNavigation
import MapboxDirections

class Trip {
    var start_time: Date?
    var end_time: Date?
    var stops: Array<Stop>?
    var stopsCoordinates: Array<CLLocationCoordinate2D>?
    var route: Route?
    var cancelled: Bool?
    var origin: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var destination: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var origin_name: String?
    var destination_name: String?
    
    init() {
        
    }
    
    func setTrip( _ trip: [String : Any?]) {
        for attr in trip {
            switch attr.key {
            case "start_time":
                self.start_time = Date(timeIntervalSince1970: attr.value as! Double)
            case "end_time":
                self.end_time = Date(timeIntervalSince1970: attr.value as! Double)
            case "stops":
                stops = []
                stopsCoordinates = []
                let tempStops = attr.value as! Array<[String:Any]>
                let tempStop = Stop()
                for stop in tempStops {
                    tempStop.setStop(stop)
                    self.stops!.append(tempStop)
                    self.stopsCoordinates!.append(tempStop.location)
                }
                stops?.sort(by: {(s1: Stop, s2: Stop) -> Bool in
                    if(s1.position! <= s2.position!) {
                        return true
                    }
                    else {
                        return false
                    }
                })
            case "arrival_time":
                print(attr.value)
            case "origin_lat":
                self.origin.latitude = attr.value as! Double
            case "origin_lon":
                self.origin.longitude = attr.value as! Double
            case "dest_lat":
                self.destination.latitude = attr.value as! Double
            case "dest_lon":
                self.destination.longitude = attr.value as! Double
            case "origin_name":
                self.origin_name = attr.value as? String
            case "dest_name":
                self.destination_name = attr.value as? String
            default:
                break
            }
        }
    }
    
    func defaultStops() {
        stops = []
        stopsCoordinates = []
        self.stops!.append(Stop(origin, 0))
        self.stops!.append(Stop(destination, 1))
        self.stopsCoordinates!.append(origin)
        self.stopsCoordinates!.append(destination)
    }
}
