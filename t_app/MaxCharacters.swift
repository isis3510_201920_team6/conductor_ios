//
//  MaxCharacters.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 17/11/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import Foundation

import UIKit
private var maxLengths = [UITextField: Int] ()

extension UITextField{

    @IBInspectable var maxLength: Int {
        get {
            guard let length = maxLengths[self] else{
                return Int.max
            }
            return length
        }
        set{
            maxLengths[self] = newValue
             addTarget(
                   self,
                   action: #selector(limitLength),
                   for: UIControl.Event.editingChanged
                 )
        }
    }
    @objc func limitLength(textField: UITextField) {
       guard let prospectiveText = textField.text,
                 prospectiveText.count > maxLength
       else {
         return
       }
       
       let selection = selectedTextRange
       let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
       text = prospectiveText.substring(to: maxCharIndex)
       selectedTextRange = selection
     }
}
