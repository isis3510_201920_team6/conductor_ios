//
//  MapViewController.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 10/8/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import UIKit
import Mapbox
import MapboxCoreNavigation
import MapboxNavigation
import MapboxDirections

class MapViewController: UIViewController, MGLMapViewDelegate {
    
    var mapView: NavigationMapView!
    var progressView: UIProgressView!
    var directionsRoute: Route?
    var currentTrip: Trip!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView = NavigationMapView(frame: view.bounds)
        view.addSubview(mapView)
        mapView.delegate = self
        //mapView.showsUserLocation = true
        //mapView.setUserTrackingMode(.follow, animated: true, completionHandler: nil)
        mapView.setCenter(currentTrip.stopsCoordinates!.first!, zoomLevel: 12, direction: 0, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackProgressDidChange), name: NSNotification.Name.MGLOfflinePackProgressChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveError), name: NSNotification.Name.MGLOfflinePackError, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(offlinePackDidReceiveMaximumAllowedMapboxTiles), name: NSNotification.Name.MGLOfflinePackMaximumMapboxTilesReached, object: nil)
        
        // Add a gesture recognizer to the map view
        //let longPress = UILongPressGestureRecognizer(target: self, action: #selector(didLongPress(_:)))
        //mapView.addGestureRecognizer(longPress)
        if(CheckInternet.Connection()) {
            calculateRoute(route: currentTrip.stopsCoordinates!) { (route, error) in
                if error != nil {
                    print("Error calculation route")
                }
            }
        } else {
            for location in currentTrip.stopsCoordinates! {
                let annotation: MGLPointAnnotation = MGLPointAnnotation()
                annotation.coordinate = location
                annotation.title = "Cosas"
                mapView.addAnnotation(annotation)
            }
        }
        
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        // Start downloading tiles and resources for z13-16.
        if(CheckInternet.Connection()) {
            startOfflinePackDownload()
        }
    }
    
    deinit {
        // Remove offline pack observers.
        NotificationCenter.default.removeObserver(self)
    }
    
    func startOfflinePackDownload() {
        // Create a region that includes the current viewport and any tiles needed to view it when zoomed further in.
        // Because tile count grows exponentially with the maximum zoom level, you should be conservative with your `toZoomLevel` setting.
        let region = MGLTilePyramidOfflineRegion(styleURL: mapView.styleURL, bounds: mapView.visibleCoordinateBounds, fromZoomLevel: 11, toZoomLevel: 16)
        
        // Store some data for identification purposes alongside the downloaded resources.
        let userInfo = ["name": "My Offline Pack"]
        let context = NSKeyedArchiver.archivedData(withRootObject: userInfo)
        
        // Create and register an offline pack with the shared offline storage object.
        MGLOfflineStorage.shared.addPack(for: region, withContext: context) { (pack, error) in
            guard error == nil else {
                // The pack couldn’t be created for some reason.
                print("Error: \(error?.localizedDescription ?? "unknown error")")
                return
            }
            
            // Start downloading.
            pack!.resume()
        }
        
    }
    
    // MARK: - MGLOfflinePack notification handlers
    
    @objc func offlinePackProgressDidChange(notification: NSNotification) {
        // Get the offline pack this notification is regarding,
        // and the associated user info for the pack; in this case, `name = My Offline Pack`
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String] {
            let progress = pack.progress
            // or notification.userInfo![MGLOfflinePackProgressUserInfoKey]!.MGLOfflinePackProgressValue
            let completedResources = progress.countOfResourcesCompleted
            let expectedResources = progress.countOfResourcesExpected
            
            // Calculate current progress percentage.
            let progressPercentage = Float(completedResources) / Float(expectedResources)
            
            // Setup the progress bar.
            if progressView == nil {
                progressView = UIProgressView(progressViewStyle: .default)
                let frame = view.bounds.size
                progressView.frame = CGRect(x: frame.width / 4, y: frame.height * 0.75, width: frame.width / 2, height: 10)
                view.addSubview(progressView)
            }
            
            progressView.progress = progressPercentage
            
            // If this pack has finished, print its size and resource count.
            if completedResources == expectedResources {
                let byteCount = ByteCountFormatter.string(fromByteCount: Int64(pack.progress.countOfBytesCompleted), countStyle: ByteCountFormatter.CountStyle.memory)
                print("Offline pack “\(userInfo["name"] ?? "unknown")” completed: \(byteCount), \(completedResources) resources")
            } else {
                // Otherwise, print download/verification progress.
                print("Offline pack “\(userInfo["name"] ?? "unknown")” has \(completedResources) of \(expectedResources) resources — \(progressPercentage * 100)%.")
            }
        }
    }
    
    @objc func offlinePackDidReceiveError(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let error = notification.userInfo?[MGLOfflinePackUserInfoKey.error] as? NSError {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” received error: \(error.localizedFailureReason ?? "unknown error")")
        }
    }
    
    @objc func offlinePackDidReceiveMaximumAllowedMapboxTiles(notification: NSNotification) {
        if let pack = notification.object as? MGLOfflinePack,
            let userInfo = NSKeyedUnarchiver.unarchiveObject(with: pack.context) as? [String: String],
            let maximumCount = (notification.userInfo?[MGLOfflinePackUserInfoKey.maximumCount] as AnyObject).uint64Value {
            print("Offline pack “\(userInfo["name"] ?? "unknown")” reached limit of \(maximumCount) tiles.")
        }
    }
    
    /*@objc func didLongPress(_ sender: UILongPressGestureRecognizer) {
     guard sender.state == .began else { return }
     
     // Converts point where user did a long press to map coordinates
     let point = sender.location(in: mapView)
     let coordinate = mapView.convert(point, toCoordinateFrom: mapView)
     
     // Create a basic point annotation and add it to the map
     let annotation = MGLPointAnnotation()
     annotation.coordinate = coordinate
     annotation.title = "Start navigation"
     mapView.addAnnotation(annotation)
     
     // Calculate the route from the user's location to the set destination
     calculateRoute(from: (mapView.userLocation!.coordinate), to: annotation.coordinate) { (route, error) in
     if error != nil {
     print("Error calculating route")
     }
     }
     
     }
     // Calculate route to be used for navigation
     func calculateRoute(from origin: CLLocationCoordinate2D,
     to destination: CLLocationCoordinate2D,
     completion: @escaping (Route?, Error?) -> ()) {
     
     // Coordinate accuracy is the maximum distance away from the waypoint that the route may still be considered viable, measured in meters. Negative values indicate that a indefinite number of meters away from the route and still be considered viable.
     //let origin = Waypoint(coordinate: origin, coordinateAccuracy: -1, name: "Start")
     //let destination = Waypoint(coordinate: destination, coordinateAccuracy: -1, name: "Finish")
     
     var route: [Waypoint] = []
     for location in test! {
     route.append(Waypoint(coordinate: location, coordinateAccuracy: -1))
     }
     
     // Specify that the route is intended for automobiles avoiding traffic
     let options = NavigationRouteOptions(waypoints: route, profileIdentifier: .automobile)
     
     // Generate the route object and draw it on the map
     _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
     print(routes!)
     self.directionsRoute = routes?.first
     // Draw the route on the map after creating it
     self.drawRoute(route: self.directionsRoute!)
     
     }
     }*/
    
    func calculateRoute(route: [CLLocationCoordinate2D],
                        completion: @escaping (Route?, Error?) -> ()) {
        
        // Coordinate accuracy is the maximum distance away from the waypoint that the route may still be considered viable, measured in meters. Negative values indicate that a indefinite number of meters away from the route and still be considered viable.
        var waypoint: [Waypoint] = []
        for location in route {
            waypoint.append(Waypoint(coordinate: location, coordinateAccuracy: -1))
            let annotation: MGLPointAnnotation = MGLPointAnnotation()
            annotation.coordinate = location
            annotation.title = "Comenzar"
            mapView.addAnnotation(annotation)
        }
        print(waypoint)
        
        // Specify that the route is intended for automobiles avoiding traffic
        let options = NavigationRouteOptions(waypoints: waypoint, profileIdentifier: .automobile)
        
        // Generate the route object and draw it on the map
        _ = Directions.shared.calculate(options) { [unowned self] (waypoints, routes, error) in
            self.directionsRoute = routes?.first
            guard let _ = self.directionsRoute else {
                let alert = UIAlertController(title: "Error recuperando la ruta", message: "Es probable que a causa sea información inconsistente.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
                return
            }
            self.currentTrip.route = self.directionsRoute!
            // Draw the route on the map after creating it
            self.drawRoute(route: self.directionsRoute!)
            
        }
    }
    
    func drawRoute(route: Route) {
        guard route.coordinateCount > 0 else { return }
        // Convert the route’s coordinates into a polyline
        var routeCoordinates = route.coordinates!
        let polyline = MGLPolylineFeature(coordinates: &routeCoordinates, count: route.coordinateCount)
        
        // If there's already a route line on the map, reset its shape to the new route
        if let source = mapView.style?.source(withIdentifier: "route-source") as? MGLShapeSource {
            source.shape = polyline
        } else {
            let source = MGLShapeSource(identifier: "route-source", features: [polyline], options: nil)
            
            // Customize the route line color and width
            let lineStyle = MGLLineStyleLayer(identifier: "route-style", source: source)
            lineStyle.lineColor = NSExpression(forConstantValue: #colorLiteral(red: 0.1897518039, green: 0.3010634184, blue: 0.7994888425, alpha: 1))
            lineStyle.lineWidth = NSExpression(forConstantValue: 3)
            
            // Add the source and style layer of the route line to the map
            mapView.style?.addSource(source)
            mapView.style?.addLayer(lineStyle)
        }
    }
    
    // Implement the delegate method that allows annotations to show callouts when tapped
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    // Present the navigation view controller when the callout is selected
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        let navigationViewController = NavigationViewController(for: directionsRoute!)
        navigationViewController.modalPresentationStyle = .fullScreen
        self.present(navigationViewController, animated: true, completion: nil)
        
    }
    
    func setTrip(trip: Trip) {
        self.currentTrip = trip
    }
    
}
