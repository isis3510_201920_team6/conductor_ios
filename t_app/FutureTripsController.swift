//
//  FutureTripsController.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 10/22/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import UIKit
import Firebase
import Mapbox

class FutureTripsController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    var trips = [Trip]()
    var currentTrip: Trip!
    let db = Firestore.firestore()
    var refreshControl = UIRefreshControl()
    
    let ftUpdated = Notification.Name(rawValue: futureTripsUpdated)
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.title = "Futuros viajes"
    }
    
    override func loadView() {
        createObservers()
        super.loadView()
        if(CheckInternet.Connection()) {
            self.loadData()
        } else {
            let alert = UIAlertController(title: "Sin conexión", message: "Si los elementos fueron descargados previamente, se utilizará la información almacenada.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                self.loadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: {action in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "tripCell")
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(FutureTripsController.futureUpdated), name: ftUpdated, object: nil)
    }
    
    @objc func futureUpdated() {
        if(CheckInternet.Connection()) {
            self.loadData()
        }
        else {
            let alert = UIAlertController(title: "Sin conexión", message: "Si los elementos fueron descargados previamente, se utilizará la información almacenada.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                self.loadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: {action in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func loadData() {
        if(globalDriver.futureTrips != nil) {
            self.trips = globalDriver.futureTrips!
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tripCell", for: indexPath)
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        let tripDate = formatter.string(from: self.trips[indexPath.row].start_time!) + "\n"
//        formatter.dateFormat = "HH:mm:ss"
//        let cellContent = formatter.string(from: self.trips[indexPath.row].start_time!) + " - " + formatter.string(from: self.trips[indexPath.row].end_time!)
        if(self.trips.isEmpty) {
            cell.textLabel?.text = "There are no trips"
        }
        else {
            cell.textLabel?.text = "from: " + self.trips[indexPath.row].origin_name! + "\nto: " + self.trips[indexPath.row].destination_name!
            cell.textLabel?.numberOfLines = 2
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(CheckInternet.Connection()) {
            let mapView: MapViewController = MapViewController()
            self.currentTrip = self.trips[indexPath.row]
            performSegue(withIdentifier: "pushMap", sender: mapView)
        } else {
            let alert = UIAlertController(title: "Sin conexión", message: "Si los elementos fueron descargados previamente, serán visibles en el mapa. La navegación está desabilitada sin una conexión a internet.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                let mapView: MapViewController = MapViewController()
                self.currentTrip = self.trips[indexPath.row]
                self.performSegue(withIdentifier: "pushMap", sender: mapView)
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: {action in
                //
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.destination is MapViewController) {
            let mapView = segue.destination as? MapViewController
            self.currentTrip.defaultStops()
            print(currentTrip.stopsCoordinates)
            mapView?.setTrip(trip: self.currentTrip)
        }
    }
    
    @objc func refresh(_ sender:AnyObject) {
        globalDriver.updateDriver()
        self.refreshControl.endRefreshing()
    }
}
