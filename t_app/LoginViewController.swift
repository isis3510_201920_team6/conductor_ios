//
//  AuthViewController.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 10/30/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtMail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnAccept: UIButton!
    private let currentDriver: Driver = Driver()
    private var loaded: Bool = false
    private var pastReady = false
    private var futureReady = false
    
    
    let dUpdated = Notification.Name(rawValue: driverUpdated)
    let dUpdating = Notification.Name(rawValue: driverUpdating)
    let ptUpdated = Notification.Name(rawValue: pastTripsUpdated)
    let ptUpdating = Notification.Name(rawValue: pastTripsUpdating)
    let ftUpdated = Notification.Name(rawValue: futureTripsUpdated)
    let ftUpdating = Notification.Name(rawValue: futureTripsUpdating)
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createObservers()
        
        view.addConstraints([
            NSLayoutConstraint(item: txtMail as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: txtMail as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.5, constant: 0),
            NSLayoutConstraint(item: txtPassword as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: txtPassword as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.75, constant: 0),
            NSLayoutConstraint(item: btnAccept as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: btnAccept as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.75, constant: 0)
        ])
        
        btnAccept.addTarget(self, action: #selector(loginAction), for: .touchUpInside)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        self.txtMail.delegate = self
        self.txtPassword.delegate = self
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func loginAction(_ sender: UIButton!) {
        if(CheckInternet.Connection()) {
            self.showSpinner(onView: self.view)
            Auth.auth().signIn(withEmail: txtMail.text!, password: txtPassword.text!) { (user, error) in
                if error == nil {
                    DispatchQueue(label: "update driver").async {
                        globalDriver.updateDriver()
                        while (!self.pastReady || !self.futureReady) {continue}
                        self.removeSpinner()
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "Bienvenido", message: "Inicio de sesión exitoso.", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                                self.performSegue(withIdentifier: "login_to_tabs", sender: UITabBarController.self)
                            }))
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
                else {
                    self.removeSpinner()
                    let alertController = UIAlertController(title: "Error", message:    error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
                    
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        else {
            self.removeSpinner()
            let alertController = UIAlertController(title: "Error en la red", message: "Por favor revise su conexión a internet e intente nuevamente.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "Aceptar", style: .cancel, handler: nil)
            
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.pastUpdated), name: ptUpdated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.futureUpdated), name: ftUpdated, object: nil)
    }
    
    @objc func futureUpdated() {
        self.futureReady = true
    }
    
    @objc func pastUpdated() {
        self.pastReady = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
