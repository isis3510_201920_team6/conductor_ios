//
//  UserInfoViewController.swift
//  t_app
//
//  Created by Mechas Zambrano on 18/11/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import Foundation
import UIKit

class UserInfoViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtID: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addConstraints([
        NSLayoutConstraint(item: txtName as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.5, constant: 0),
        NSLayoutConstraint(item: txtID as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.75, constant: 0),
        NSLayoutConstraint(item: txtPhone as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: btnContinue as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: btnContinue as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.75, constant: 0),
        ])
        
        btnContinue.addTarget(self, action: #selector(continueAction), for: .touchUpInside)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        
        view.addGestureRecognizer(tap)
        
        self.txtName.delegate = self
        self.txtID.delegate = self
        self.txtPhone.delegate = self
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func continueAction(sender: UIButton!) {
        if (txtName.text != "" && txtPhone.text != "" && txtID.text != "") {
            guard let _ = Int(txtPhone.text!) else {
                let alertController = UIAlertController(title: "Información inválida", message: "El teléfono debe ser un número.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            guard let _ = Int(txtID.text!) else {
                let alertController = UIAlertController(title: "Información inválida", message: "La identificación debe ser un número.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
                return
            }
            self.performSegue(withIdentifier: "show_form", sender: RegisterViewController.self)
        }
        else {
            let alertController = UIAlertController(title: "Falta información", message: "Por favor llene los todos los espacios.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.destination is RegisterViewController) {
            let registerView = segue.destination as? RegisterViewController
            registerView?.setUserData(txtName.text!, txtID.text!, txtPhone.text!)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
