//
//  Constants.swift
//  t_app
//
//  Created by Mechas Zambrano on 3/12/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import Foundation

let driverUpdated = "driverUpdated"
let driverUpdating = "driverUpdating"
let pastTripsUpdated = "pastTripsUpdated"
let pastTripsUpdating = "pastTripsUpdating"
let futureTripsUpdated = "futureTripsUpdated"
let futureTripsUpdating = "futureTripsUpdating"
let globalDriver = Driver()
