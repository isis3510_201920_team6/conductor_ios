//
//  DriverViewController.swift
//  t_app
//
//  Created by ESTEBAN GALAN ZAMBRANO on 10/17/19.
//  Copyright © 2019 isis3510. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage

class DriverViewController: UIViewController {
    
    @IBOutlet weak var imgDriver: UIImageView!
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet weak var lblDriverScore: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var btnLogout: UIButton!
    
    
    let db = Firestore.firestore()
    let storage = Storage.storage()
    var driverImage: UIImage?
    
    let dUpdated = Notification.Name(rawValue: driverUpdated)
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.title = "Perfil"
    }
    
    override func loadView() {
        createObservers()
        super.loadView()
        if(CheckInternet.Connection()) {
            self.loadData()
        } else {
            let alert = UIAlertController(title: "Sin conexión", message: "Si los elementos fueron descargados previamente, se utilizará la información almacenada.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                self.loadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: {action in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addConstraints([
            NSLayoutConstraint(item: lblDriverName as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.5, constant: 0),
            NSLayoutConstraint(item: lblDriverName as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.25, constant: 0),
            NSLayoutConstraint(item: lblName as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 0.5, constant: 0),
            NSLayoutConstraint(item: lblName as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.25, constant: 0),
            NSLayoutConstraint(item: lblDriverScore as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1.5, constant: 0),
            NSLayoutConstraint(item: lblDriverScore as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.5, constant: 0),
            NSLayoutConstraint(item: lblScore as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 0.5, constant: 0),
            NSLayoutConstraint(item: lblScore as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.5, constant: 0),
            NSLayoutConstraint(item: imgDriver as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: imgDriver as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.5, constant: 0),
            NSLayoutConstraint(item: btnLogout as Any, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: btnLogout as Any, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1.75, constant: 0)])
        
        btnLogout.addTarget(self, action: #selector(logoutAction), for: .touchUpInside)
    }
    
    @objc func logoutAction() {
        let alert = UIAlertController(title: "Cerrando sesión", message: "¿Está seguro que desea cerrar sesión?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Sí", style: .default, handler: { action in
            do {
                try Auth.auth().signOut()
            } catch let signOutError as NSError {
              print ("Error signing out: %@", signOutError)
            }
            self.performSegue(withIdentifier: "logout", sender: UIStoryboard.self)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(DriverViewController.updated), name: dUpdated, object: nil)
    }
    
    @objc func updated() {
        if(CheckInternet.Connection()) {
            self.loadData()
        }
        else {
            let alert = UIAlertController(title: "Sin conexión", message: "Si los elementos fueron descargados previamente, se utilizará la información almacenada.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                self.loadData()
            }))
            alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: {action in
                self.navigationController?.popViewController(animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func loadData() {
        self.lblDriverName.text = globalDriver.name
        let score = globalDriver.score! as NSNumber
        self.lblDriverScore.text = String(Double(truncating: score))
        let httpsReference = self.storage.reference(forURL: globalDriver.profilePic!)
        httpsReference.getData(maxSize: 1 * 2048 * 2048) { (data, error) in
            if(error != nil) {
                print("no profile picture to display")
            } else {
                self.driverImage = UIImage(data: data!)
                self.updateImage()
            }
        }
    }
    
    func updateImage() {
        imgDriver.frame = imgDriver.frame(forAlignmentRect: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height*0.5))
        UIGraphicsBeginImageContext(CGSize(width: imgDriver.frame.width, height: imgDriver.frame.height))
        driverImage!.draw(in: CGRect(x: 0, y: 0, width: imgDriver.frame.width, height: imgDriver.frame.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        imgDriver.image = newImage
    }
}
